/** @type {import('tailwindcss').Config} */
const customeClass = require('./src/theme')
module.exports = {
  content: [
    "./src/**/*.{js,jsx,ts,tsx}",
  ],
  theme: {
    extend: { ...customeClass},
  },
  plugins: [],
}
