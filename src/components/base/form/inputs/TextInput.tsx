import classNames from "classnames";
import { useField } from "formik";
import React, { forwardRef, memo } from "react";
import { InputCommonType, TextInputCommonType } from "./input.type";


export type TextInputProps = TextInputCommonType & InputCommonType;

export const TextInput = memo(
    forwardRef(
        (
            {
                rootClasses,
                labelClasses,
                inputClasses,
                errorClasses,
                wrapperClasses,
                EndAdornment,
                StartAdornment,
                label,
                id,
                required,
                meta,
                errorMessage,
                type = "text",
                ...props
            }: Omit<TextInputProps,"name">&{name?:string},
            ref
        ) => {
            return (
                <div className={classNames(rootClasses)}>
                    {label && (
                        <label
                            {...(id && { htmlFor: id })}
                            className={classNames(
                                labelClasses,
                                meta?.touched && meta?.error ? "textError" : ""
                            )}
                        >
                            {required && <span className="textError">* </span>}
                            {label}
                        </label>
                    )}
                    <div
                        className={classNames(
                            wrapperClasses,
                            meta?.touched && meta?.error
                                ? "emailInputLoginError"
                                : "emailInputLogin",
                            "flex items-center justify-center pr-2"
                            // "focus-within:ring focus-within:ring-blue-500"
                        )}
                    >
                        {StartAdornment && <StartAdornment />}

                        <input
                            ref={ref as any}
                            {...(id && { id })}
                            type={type}
                            className={classNames(
                                inputClasses,
                                "py-4 border-b-6 border-gray-500 text-formText placeholder-formText  bg-placeholder text-lg font-bold pl-4"
                            )}
                            {...props}
                        />
                        {EndAdornment && <EndAdornment />}
                    </div>

                </div>
            );
        }
    )
);

export const TextInputFormik = forwardRef((props: TextInputProps, ref) => {
    const [field, meta] = useField(props.name);
    return <TextInput {...props} {...field} meta={meta} ref={ref as any} />;
});
