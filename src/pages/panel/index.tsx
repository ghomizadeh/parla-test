// @flow
import * as React from 'react';
import PanelLayout from "../../components/layouts/panel";

type Props = {

};
export const PanelIndex = (props: Props) => {
    return (
        <PanelLayout title={"Panel"}>
            <section className={"h-screen"}><h1 className={"text-black p-24 text-lg "}>Panel Index</h1></section>
        </PanelLayout>
    );
};
