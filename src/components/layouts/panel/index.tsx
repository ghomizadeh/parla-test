import React, {Fragment, ReactNode} from "react";
import {Header} from "./partials/header";
import {Sidebar} from "./partials/sidebar"
interface Props {
    children?: ReactNode|JSX.Element|JSX.Element[];
    title?:string;
}

export default function PanelLayout(props: Props) {
    const { children,title } = props
    return (
        <Fragment>
           <div className={"flex"}>
               <Sidebar/>
               <section className={"flex flex-col w-full"}>
                   <Header title={title}/>
                   <main>
                       {children}
                   </main>
               </section>
           </div>
        </Fragment>
    )
}
