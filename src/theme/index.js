const colors = require('./colors');
const spacing = require('./spacing');
const borderWidth = require('./borderWidth');
module.exports = {
    colors,
    spacing,
    borderWidth
};
