import classNames from "classnames";
import React from "react";
type BaseButtonType = React.DetailedHTMLProps<
    React.ButtonHTMLAttributes<HTMLButtonElement>,
    HTMLButtonElement
    >;

interface Props extends BaseButtonType {
    text?: string;
}
export const FormButton = ({text,children,className,...props}: Props) => {
    return (
        <button
            {...props}
            className={classNames(className)}
        >
                    {text && <span className={""}>{text}</span>}
                    {children && children}
        </button>
    );
};
