// @flow 
import * as React from 'react';
import {Logo} from "../../../base/logo";
import NLink from "../../../base/nLink";

type Props = {
    
};
export const Sidebar = (props: Props) => {
    return (
        <aside className={"bg-parlay w-42"}>
            <Logo/>
            <ul className={"flex flex-col gap-y-6 pl-6 text-lg mt-4 text-asideText font-medium"}>
                <li><NLink to={"/panel/profile"} title={"Site Profile"} activeClass={"white"} /></li>
                <li><NLink to={"/panel/consent"} title={"Consent Forms"} activeClass={"white"} /></li>
                <li><NLink to={"/panel/members"} title={"Site Members"} activeClass={"white"} /></li>
            </ul>
        </aside>
    );
};
