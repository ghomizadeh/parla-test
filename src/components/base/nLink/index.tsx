// @flow
import * as React from 'react';
import {NavLink} from "react-router-dom";

type Props = {
    title?:string;
    to:string;
    activeClass?:string;
};
 const NLink = (props: Props) => {
    const {title,to,activeClass} = props;
    return (
        <NavLink
            to={to}
            style={
                ({isActive}) => (
                    isActive
                        ? {
                            textDecoration: 'none',
                            color: activeClass
                        }
                        :{}
                )
            }
        >
            {title}
        </NavLink>
    );
};
export default NLink;
