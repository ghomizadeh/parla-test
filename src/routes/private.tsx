import React from "react";
import { Navigate, Outlet } from 'react-router-dom';
const PrivateRoute =  () => {
    const isLogin:any = false; //you can set with token
    return !isLogin  ? <Outlet /> : <Navigate to="/" />;
}
export default PrivateRoute;
