// @flow 
import * as React from 'react';
import PanelLayout from "../../../components/layouts/panel";
import {TextInputFormik} from "../../../components/base/form/inputs/TextInput";
import * as Yup from "yup";
import {Form, Formik} from "formik";
import {FormButton} from "../../../components/base/form/buttons";
const INITIAL_VALUES = {
    prefix: "",
    name: "",
    middleName: "",
    email: "",
    lastName: "",
    birthday: "",
};
const validationSchema = Yup.object().shape({
    prefix: Yup.string().required("This field cannot be empty"),
    name: Yup.string().required("This field cannot be empty"),
    email: Yup.string().required("This field cannot be empty"),
    middleName: Yup.number().required("This field cannot be empty"),
    lastName: Yup.number().required("This field cannot be empty"),
    birthday: Yup.number().required("This field cannot be empty"),

});
export const Members = () => {

    const onSubmit = (values: typeof INITIAL_VALUES) =>{
        console.log("salam")
    }

    return (
        <PanelLayout title={"Register New Site"}>
            <section className={"px-20 py-14 pr-52 h-screen"}>
                <h1 className={"text-xl font-bold text-formText"}>Site Lead*</h1>
                <div className={"mt-12"}>
                    <Formik
                        validationSchema={validationSchema}
                        onSubmit={onSubmit}
                        initialValues={INITIAL_VALUES}
                    >
                        <Form >
                           <section className={"flex"}>
                               <div className={"flex flex-col gap-y-4"}>
                                   <div className={"flex"}>
                                       <TextInputFormik
                                           name="prefix"
                                           required
                                           placeholder="Prefix(?)"
                                           inputClasses={"w-28"}

                                       />
                                       <TextInputFormik
                                           name="name"
                                           required
                                           placeholder="First Name"
                                           inputClasses={"w-80"}
                                       />
                                   </div>
                                   <TextInputFormik
                                       name="lastName"
                                       required
                                       placeholder="Last Name*"
                                       inputClasses={"w-full"}
                                   />
                                   <TextInputFormik
                                       name="birthday"
                                       type={"date"}
                                       required
                                       placeholder="Month and day of birth (MM/DD)*"
                                       inputClasses={"w-full"}
                                   />
                               </div>
                               <div className={"flex flex-col gap-y-4 w-full"}>
                                   <TextInputFormik
                                       name="middleName"
                                       required
                                       placeholder="Middle name"
                                       inputClasses={"w-full"}
                                   />
                                   <TextInputFormik
                                       name="email"
                                       type={"email"}
                                       required
                                       placeholder="E-mail*"
                                       inputClasses={"w-full"}
                                   />
                               </div>
                           </section>
                            <section>
                                <div className={"flex gap-1 text-formText font-bold text-xl my-8"}>
                                    <span>+</span>
                                    <span>Add a Site Admin (optional)</span>
                                </div>
                            </section>
                            <section className={" flex flex-col gap-y-4 ml-4"}>
                                <div>
                                    <label>
                                        <input type="checkbox" className="accent-formText w-4 h-4" /> <span className={"text-xl font-bold ml-2"}>Enable Multi-Factor Authentication</span>
                                    </label>
                                </div>
                                <div className={"text-black text-sm font-bold"}>
                                    Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut
                                    enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.
                                </div>
                            </section>
                            <section className={"flex justify-between items-center border-t border-formText mt-20 py-10"}>
                                <FormButton type={"button"} text={"Back"} className={"text-xl text-black font-bold border border-3 border-black py-3 px-8"}/>
                                <FormButton text={"Continue"} className={"text-lg text-white bg-btnAction font-medium  py-3 px-8"} />
                            </section>

                        </Form>
                    </Formik>

                </div>

            </section>
        </PanelLayout>
    );
};
