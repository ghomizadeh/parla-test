// @flow
import * as React from 'react';
import PanelLayout from "../../../components/layouts/panel";

type Props = {

};
export const Profile = (props: Props) => {
    return (
        <PanelLayout title={"Site Profile"}>
            <section className={"h-screen"}><h1 className={"text-black  p-24 text-lg "}>Profile Index</h1></section>
        </PanelLayout>
    );
};
