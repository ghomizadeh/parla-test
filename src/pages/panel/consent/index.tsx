// @flow
import * as React from 'react';
import PanelLayout from "../../../components/layouts/panel";

type Props = {

};
export const Consent = (props: Props) => {
    return (
        <PanelLayout title={"Consent Forms"}>
            <section className={"h-screen"}><h1 className={"text-black  p-24 text-lg "}>Consent Index</h1></section>
        </PanelLayout>
    );
};
