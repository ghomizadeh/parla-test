import React from "react";
import { BrowserRouter, Route, Routes,Navigate } from 'react-router-dom';


import PrivateRoute from "./private";
import {PanelIndex} from '../pages/panel';
import {Members} from "../pages/panel/members";
import {Login} from "../pages/login";
import {Profile} from "../pages/panel/profile";
import {Consent} from "../pages/panel/consent";


const Router = () => {

    return (
        <BrowserRouter>
            <Routes>
                <Route  path="/" element={<Login />} />
                <Route  path='/panel' element={<PrivateRoute/>}>
                    <Route  path="/panel" element={<PanelIndex />} />
                    <Route  path="/panel/profile" element={<Profile />} />
                    <Route  path="/panel/consent" element={<Consent />} />
                    <Route  path="/panel/members" element={<Members />} />
                </Route>
            </Routes >
        </BrowserRouter>
    );
};

export default Router;
