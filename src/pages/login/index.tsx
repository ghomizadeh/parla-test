// @flow 
import * as React from 'react';
import {TextInputFormik} from "../../components/base/form/inputs/TextInput";
import {Form, Formik} from "formik";
import {FormButton} from "../../components/base/form/buttons";
import * as Yup from "yup";
const INITIAL_VALUES = {
    username: "",
    password: "",
};
const validationSchema = Yup.object().shape({
    username: Yup.string().required("This field cannot be empty"),
    password: Yup.string()
        .required("This field cannot be empty")
        .min(8, "This field must min 8 charcter"),
});
export const Login = () => {
    const onSubmit = () =>{
        window.location.href = "/panel"
    }
    return (
        <section className={"flex items-center justify-center"}>
            <main className={"py-32"}>
                <h1 className={"text-4xl text-black"}>LOGIN</h1>
                <div className={"mt-12 w-96"}>
                    <Formik
                        validationSchema={validationSchema}
                        onSubmit={onSubmit}
                        initialValues={INITIAL_VALUES}
                    >
                        <Form>
                            <div className={"flex flex-col gap-y-4 w-full"}>
                                    <TextInputFormik
                                        name="username"
                                        required
                                        placeholder="Prefix(?)"
                                        inputClasses={"w-full"}

                                    />
                                    <TextInputFormik
                                        name="password"
                                        required
                                        type={"password"}
                                        placeholder="Password"
                                        inputClasses={"w-full"}
                                    />
                            </div>
                            <section className={"flex justify-center items-center border-t border-formText mt-20 py-10"}>
                                <FormButton text={"Login"} className={"text-lg text-white bg-btnAction font-medium  py-3 px-8"} onClick={()=>onSubmit}/>
                            </section>

                        </Form>
                    </Formik>

                </div>

            </main>
        </section>
    );
};
