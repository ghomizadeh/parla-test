import * as React from 'react';
type Props = {
    title?:string
};
export const Header = (props: Props) => {
    const {title} = props;
    return (
        <header className={"bg-parlay py-6"}>
            <div>
                <h1 className={"text-white text-xl font-medium pl-6"}>{title}</h1>
            </div>
        </header>
    );
};
